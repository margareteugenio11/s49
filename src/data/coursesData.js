const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Nulla, impedit exercitationem qui laudantium quam consequatur, voluptatibus. Voluptatum praesentium fugit nobis dolores consequatur labore reiciendis sed quos quod, eum qui alias",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Nulla, impedit exercitationem qui laudantium quam consequatur, voluptatibus. Voluptatum praesentium fugit nobis dolores consequatur labore reiciendis sed quos quod, eum qui alias",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java- Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Nulla, impedit exercitationem qui laudantium quam consequatur, voluptatibus. Voluptatum praesentium fugit nobis dolores consequatur labore reiciendis sed quos quod, eum qui alias",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;
